from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class LoginForm(forms.Form):
    username = forms.CharField(label="Username", max_length=30)
    password = forms.CharField(label="Password", widget=forms.PasswordInput)

class SignupForm(UserCreationForm):
    email = forms.EmailField(label="Email address")
    gender = forms.CharField()

class ModifyAccountForm(forms.Form):
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    gender = forms.CharField(required=False)
    twitter = forms.CharField(required=False)
    new_password = forms.CharField(required=False)
    new_password2= forms.CharField(required=False)
    password     = forms.CharField(required=True)

class NewNewsForm(forms.Form):
    title = forms.CharField(max_length=200)
    content = forms.CharField()
