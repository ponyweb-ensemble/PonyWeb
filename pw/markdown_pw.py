from bs4 import BeautifulSoup
from markdown import markdown as md
def markdown_pw(s):
    h = BeautifulSoup(md(s))
    for node in h.find_all('h1'): node['class'] = "ui header"
    for node in h.find_all('h2'): node['class'] = "ui header"
    for node in h.find_all('ul'): node['class'] = "ui bulleted list"
    for node in h.find_all('li'): node['class'] = "item"
    return h.prettify()

sample_text = """# Header

Salut, ceci *est* le premier paragraphe.

 * Salut
 * Test

Et ça, c'est le **deuxième** paragraphe."""
