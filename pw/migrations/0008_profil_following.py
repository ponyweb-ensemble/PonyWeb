# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pw', '0007_auto_20150607_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='profil',
            name='following',
            field=models.ManyToManyField(to='pw.Profil', related_name='following_rel_+'),
        ),
    ]
