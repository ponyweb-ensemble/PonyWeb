# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pw', '0002_profil_presentation'),
    ]

    operations = [
        migrations.AddField(
            model_name='profil',
            name='show_mail',
            field=models.BooleanField(default=False),
        ),
    ]
