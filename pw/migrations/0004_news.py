# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pw', '0003_profil_show_mail'),
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=100)),
                ('content', models.TextField(null=True)),
                ('date', models.DateTimeField(verbose_name='Publication Date', auto_now_add=True)),
                ('nsfw', models.BooleanField(default=False)),
                ('author', models.ManyToManyField(to='pw.Profil')),
            ],
        ),
    ]
