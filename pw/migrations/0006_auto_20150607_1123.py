# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pw', '0005_auto_20150605_1815'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='news',
            name='nsfw',
        ),
        migrations.AlterField(
            model_name='news',
            name='title',
            field=models.CharField(max_length=200),
        ),
    ]
