# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pw', '0008_profil_following'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profil',
            name='following',
        ),
        migrations.AddField(
            model_name='profil',
            name='following_book',
            field=models.ManyToManyField(to='pw.Profil', related_name='following_book_rel_+', blank=True),
        ),
        migrations.AddField(
            model_name='profil',
            name='following_news',
            field=models.ManyToManyField(to='pw.Profil', related_name='following_news_rel_+', blank=True),
        ),
        migrations.AddField(
            model_name='profil',
            name='following_pics',
            field=models.ManyToManyField(to='pw.Profil', related_name='following_pics_rel_+', blank=True),
        ),
        migrations.AddField(
            model_name='profil',
            name='following_sound',
            field=models.ManyToManyField(to='pw.Profil', related_name='following_sound_rel_+', blank=True),
        ),
        migrations.AlterField(
            model_name='profil',
            name='presentation',
            field=models.CharField(null=True, blank=True, max_length=400),
        ),
        migrations.AlterField(
            model_name='profil',
            name='twitter',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
