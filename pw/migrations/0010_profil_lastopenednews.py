# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('pw', '0009_auto_20150617_1221'),
    ]

    operations = [
        migrations.AddField(
            model_name='profil',
            name='lastopenednews',
            field=models.DateTimeField(default=datetime.datetime(2015, 6, 17, 13, 6, 20, 760628, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
