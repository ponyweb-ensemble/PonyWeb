from django.shortcuts import render, redirect                                                          
from django.core.urlresolvers import reverse
from django.http import HttpResponse, JsonResponse, QueryDict
from django.views.decorators.csrf import csrf_exempt
# News import
from django.utils import timezone
# My own imports
from .models import User, Profil, News
from .markdown_pw import markdown_pw as md

def home(request):
    return JsonResponse({"none":True})

@csrf_exempt
def view_news(request, id_news):
    PUT = QueryDict(request.read())
    news = News.objects.get(id=id_news)
    if request.method == "GET":
        return JsonResponse({"title":news.title, "author":news.author.id, "content":news.content, "date":news.date})
    elif request.method == "PUT":
        if not "content" in PUT:
            return JsonResponse({"error_message": "You did not provide enough information for the modification of the news.", "code":400})
        news.content = PUT['content']
        news.save()
        return JsonResponse({"error_message":"Well…none.", "code":200})
    elif request.method == "DELETE":
        news.delete()
        return JsonResponse({"error_message":"Well…none.", "code":200})

@csrf_exempt
def write_news(request):
    if request.method == "POST":
        if not (("title" in request.POST) and ("content" in request.POST)):
            return JsonResponse({"error_message": "You did not provide enough information for the creation of the news.", "code":400})
        news = News.objects.create()
        news.author = Profil.objects.get(id=1)
        news.content = request.POST['content']
        news.title = request.POST['title']
        news.date = timezone.now()
        news.save()
        return JsonResponse({"error_message":"Well…none.", "code":201, "id_news":news.id})
    else:
        return JsonResponse({"error_message":"You should use a POST request to write a news", "code":405}, status=405)
