# General imports
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
# Account imports
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import check_password
# News import
from django.utils import timezone
# Safety imports
from django.utils.html import escape
# My own imports
from .forms import LoginForm, SignupForm, ModifyAccountForm, NewNewsForm
from .models import User, Profil, News
from .markdown_pw import markdown_pw as md
# General Views
def home(request):
    return render(request, "home.html", {"mode":"home"})

# Account views
def log_in(request):
    error = False
    next = None
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                if "next" in request.GET:
                    return redirect(request.GET['next'])
                else:
                    return redirect(reverse(home))
            else:
                error = True
        else:
            print("WTF?!")
    else:
        if "next" in request.GET:
            next = request.GET['next']
        form = LoginForm()
    return render(request, "login.html", {'mode':"login","form":form,"error":error,"next":next})

def signup(request):
    error = False
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data['username'])
            gender = [i for i in [0,1,2,3] if ['other','woman', 'man', 'none'][i] == form.cleaned_data['gender']][0]
            user = User.objects.create_user(
                username = form.cleaned_data['username'],
                email    = form.cleaned_data['email'],
                password = form.cleaned_data['password1'])
            user.save()
            profil = Profil(user=user, gender=gender, twitter="")
            profil.save()
            login(request, authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password1']))
            return redirect(reverse(home))
        else:
            error = True
    else:
        form = SignupForm()
    return render(request, "signup.html",{'mode':"signup","error":error})

def log_out(request):
    logout(request)
    return redirect(reverse(home))

@login_required
def account(request):
    error = False
    profile = Profil.objects.get(user=request.user)
    if request.method == "POST":
        print(request.POST)
        form = ModifyAccountForm(request.POST)
        if form.is_valid():
            print("Valid !")
            if check_password(form.cleaned_data['password'], request.user.password):
                if form.cleaned_data['first_name']!='': request.user.first_name=form.cleaned_data['first_name']
                if form.cleaned_data['last_name'] !='': request.user.last_name= form.cleaned_data['last_name']
                if form.cleaned_data['new_password'] != '':
                    if form.cleaned_data['new_password'] == form.cleaned_data['new_password2']:
                        request.user.set_password(form.cleaned_data['new_password'])
                    else:
                        return render(request, "modify_account.djhtml", {"error":"different_passwords", "profile":profile, "mode":"editaccount"})
                if form.cleaned_data['gender'] != '':
                    profile.gender = form.cleaned_data['gender']
                if form.cleaned_data['twitter'] != '':
                    profile.twitter = form.cleaned_data['twitter']
                request.user.save()
                profile.save()
                return render(request, "modify_account.html", {"error":"none", "profile":profile, "mode":"editaccount"})
            else:
                return render(request, "modify_account.html", {"error":"bad_password", "profile":profile, "mode":"editaccount"})
        else:
            print("Not Valid !")
            return render(request, "modify_account.html", {"error":"no_password", "profile":profile, "mode":"editaccount"})
    return render(request, "modify_account.html", {"profile":profile, "mode":"editaccount"})

def view_account(request, username = None, id_user = None):
    try:
        if id_user != None:
            user = User.objects.get(id=id_user)
        else:
            user = User.objects.get(username=username)
        profile = Profil.objects.get(user=user)
        if user.id == request.user.id:
            return render(request, "view_account.html", {"mode":"self", "profile":profile, "following_news":profile.following_news.all()})
        else:
            return render(request, "view_account.html", {"profile":profile, "following_news":profile.following_news.all()})
    except User.DoesNotExist:
        return render(request, "view_account.html", {"notfound":True})
    except Profil.DoesNotExist:
        return render(request, "view_account.html", {"notfound":True})
# News views
@login_required
def news(request):
    return render(request, "news_home.html", {"mode":"news", "submode":"home"})

@login_required
def write_news(request):
    if request.method == "POST":
        form = NewNewsForm(request.POST)
        if form.is_valid():
            new = News(author=Profil.objects.get(user=request.user), title=form.cleaned_data['title'], content=escape(form.cleaned_data['content']), date=timezone.now())
            new.save()
            return redirect(reverse(news))
    return render(request, "write_news.html", {"mode":"news", "submode":"write"})

@login_required
def subscriptions(request):
    return render(request, "subscriptions.html", {"mode":"news", "submode":"subscriptions"})

def view_news(request, id):
    return render(request, "view_news.html", {"news":{"content":md(News.objects.get(id=id).content), "title":News.objects.get(id=id).title, "author":News.objects.get(id=id).author.user.username}})
