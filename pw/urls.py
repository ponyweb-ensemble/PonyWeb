from django.conf.urls import include, url
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^login', views.log_in),
    url(r'^signup', views.signup),
    url(r'^logout', views.log_out),
    url(r'^tos', TemplateView.as_view(template_name="tos.djhtml")),
    url(r'^account/$', views.account),
    url(r'^account/(?P<username>[A-Za-z_][A-Za-z0-9_]+)', views.view_account),
    url(r'^account/(?P<id_user>[0-9]+)', views.view_account),
    # API
    url(r'^api/', include('pw.api_urls')),
    # News
    url(r'^news/$', views.news),
    url(r'^news/subscriptions$', views.subscriptions),
    url(r'^news/write$', views.write_news),
    url(r'^news/(?P<id>[0-9]+)', views.view_news)
]
