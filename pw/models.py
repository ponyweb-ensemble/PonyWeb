from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Profil(models.Model):
    user           = models.OneToOneField(User)
    gender         = models.IntegerField()
    twitter        = models.CharField(max_length=50, blank=True)
    show_mail      = models.BooleanField(default=False)
    presentation   = models.CharField(max_length=400, blank=True, null=True)
    following_news = models.ManyToManyField("self", blank=True)
    following_pics = models.ManyToManyField("self", blank=True)
    following_book = models.ManyToManyField("self", blank=True)
    following_sound= models.ManyToManyField("self", blank=True)
    lastopenednews = models.DateTimeField(auto_now_add=True, auto_now=False)
    def __str__(self):
        return self.user.username


class News(models.Model):
    title  = models.CharField(max_length=200)
    author = models.ForeignKey(Profil, default=0)
    content= models.TextField(null=True)
    date   = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Publication date")
    def __str__(self):
        return self.title
