from django.conf.urls import include, url
from . import api_views
urlpatterns = [
    url('^$', api_views.home),
    # News
    url('^news/write', api_views.write_news),
    url('^news/(?P<id_news>[0-9]+)', api_views.view_news)
]
