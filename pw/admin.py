from django.contrib import admin
from .models import Profil, News
# Register your models here.
admin.site.register(Profil)

@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    def __str__(self):
        return "New"
