from django import template
from django.utils.html import escape
from django.utils.safestring import mark_safe
register = template.Library()

@register.filter(is_safe = True)
def userize(texte, floating=None):
    return mark_safe("<a href=\"/account/{}\">&amp;{}</a>".format(escape(texte), escape(texte)))

@register.filter(is_safe = True)
def twitterize(texte):
    return mark_safe("<a href=\"http://www.twitter.com/{}\">@{}</a>".format(escape(texte), escape(texte)))
